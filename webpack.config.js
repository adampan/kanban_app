const merge = require('webpack-merge');
const path = require('path');
const webpack = require('webpack');

const TARGET = process.env.npm_lifecycle_event;
const PATHS = {
  app: path.join(__dirname, 'app'),
  build: path.join(__dirname, 'build')
};

// We want to add our TARGET dependent babel configuration flag here
process.env.BABEL_ENV = TARGET;

const common = {
  // Resolving allows us to refer to files with a '.js' or a '' extension
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  // Entry accepts a path or an object of entries. We'll be using the
  // latter form given it's convenient with more complex configurations
  entry: {
    app: PATHS.app
  },
  output: {
    path: PATHS.build,
    filename: 'bundle.js'
  }
}

// Depending on our environment, we'll serve different configurations
// to webpack
if(TARGET === 'start' || !TARGET){
  module.exports = merge(common, {
    devTool: 'eval-source-map',
    devServer: {
      contentBase: PATHS.build,

      // Enable history API fallback so that HTML5 based routing works
      historyApiFallback: true,
      // This just enables HMR on the server
      // We need babel-preset-react-hmre for the client to handle the changes
      hot: true,
      inline: true,
      progress: true,

      // display errors only so reduce the amount of output
      stats: 'errors-only',

      // parse the host and port from env so that it's easy to customize
      host: process.env.HOST,
      port: process.env.PORT
    },
    module: {
      loaders: [
        {
          // test requires a regex expression
          test: /\.css$/,
          loaders: ['style', 'css'],
        },
        // Setting up jsx loader
        {
          test: /\.jsx$/,
          // Enable caching for improved performance during development
          loaders: ['babel?cacheDirectory'],
          // Remember that we only want to parse the app files
          // We don't want to include all of the node_module files as well
          include: PATHS.app
        }
      ]
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin()
    ]
  });
}

if(TARGET === 'build'){
  module.exports = merge(common, {});
}
