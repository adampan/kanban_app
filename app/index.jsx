// Remember that import is es6 - same functionality as
// require.js
import './main.css';

import React from 'react';
import ReactDOM from 'react-dom';
// If it's not a module, remember to include the relative path
import App from './components/App.jsx';

// ReactDOM is the entry point for DOM-related rendering paths
// Normally, it should be used for isomorphic React
ReactDOM.render(<App />, document.getElementById('app'));
