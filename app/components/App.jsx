// Universally unique identifier
import uuid from 'node-uuid';
import React, {Component} from 'react';

import Notes from './Notes.jsx';

export default class App extends Component {
  constructor(props) {
    // we usually call super() but super(props) makes this.props avaliable in
    // the constructor
    super(props);

    // Initializing the state of the App component
    this.state = {
      notes: [
        {
          id: uuid.v4(),
          task: 'Learn Webpack'
        },
        {
          id: uuid.v4(),
          task: 'Learn React'
        },
        {
          id: uuid.v4(),
          task: 'Do laundry'
        }
      ]
    }
  }

  // The render method looks at this.props and this.sate and returns
  // a signel child element
  render() {
    // Getting the notes from the state
    const notes = this.state.notes;

    return (
      <div>
        <button onClick={this.addNote}>+</button>
        <Notes notes={notes} onEdit="this.EditNote"/>
      </div>
    );
  }

  // We are using the property initializer here. It lets us bind the method
  // `this` to point at our *App* instance
  addNote = () => {
    this.setState({
      notes: this.state.notes.concat([{
        id: uuid.v4(),
        task: 'New task'
      }])
    })
  };

  editNote = (id, task) => {
    // Do not modify if trying to set an empty value
    if(!task.trim()) {
      return;
    }

    const notes = this.state.notes.map(note => {
      if(note.id === id){
        note.task = task;
      }
    });

    this.setState({notes})
  }
}
