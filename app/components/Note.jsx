// We still need to import react here because the jsx is getting transformed
// into calls here
import React, {Component} from 'react';

// export default ({task}) => <div>{task}</div>
// This call is the same as
// export default (props) => <div>{props.task}</div>

export default class Note extends Component {
  constructor(props) {
    super();

    // Setting the state to track the editing state
    this.state = {
      editing: false
    };
  }

  render(){
    if(this.state.editing){
      return this.renderEdit();
    }

    return this.renderNote();
  }

  renderEdit = () => {
    // We are dealing with blur and input handlers which map to DOM events
    // We also set the selection to an input using a callback at ref
    // It gets triggered after a component is mounted
    //
    // We could also use a string reference (i.e., `ref="input"`)
    // and then refer to the element in quiestion later in the code through
    // `this.refs.input`. We could get the value of the input using
    // `this.refs.input.value` through DOM in this case.
    //
    // Refs allow us to access the underlying DOM structure. They can
    // be used when you need to move beyond pure React. They also tie
    // your implementation to the browser though.
    return <input type="text"
      ref={
        element => element ?
        element.selectionStart = this.props.task.length :
        null
      }
      autoFocus={true}
      defaultValue={this.props.task}
      onBlur={this.finishEdit}
      onKeyPress={this.checkEnter} />;
  };

  renderNote = () => {
    // If the user clicks a normal note, trigger editing logic
    return <div onClick={this.edit}>{this.props.task}</div>
  }

  edit = () => {
    this.setState({
      editing: true
    })
  }

  checkEnter = (e) => {
    // If the user hits *enter*, we will stop editing
    if(e.key === `Enter`){
      this.finishEdit(e);
    }
  }

  finishEdit = (e) => {
    // `Note` will trigger an optional `onEdit` callback once it
    // has a new value. We will use this to communiate the change to `App`
    //
    // A smarter way to deal with the default value would be to set it
    // through `defaultProps`
    const value = e.target.value;

    if(this.props.onEdit) {
      this.props.onEdit(value);

      // Exiting edit mode
      this.setState({
        editing: false
      });
    }

  }
}
